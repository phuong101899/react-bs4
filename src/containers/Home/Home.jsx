import React, { Component, Fragment } from 'react';
import { Title } from '../../components/Title';
import { Image } from '../../components/Image';
import { Transformation } from 'cloudinary-react';

import style from './Home.scss';

class Home extends Component {
    render() {
        return (
            <Fragment>
                <div className="container">

                    <div className={`row py-lg-5 ${style.register}`} id="register">
                        <div className="col-12 text-center">
                            <Title>Trợ lý đắc lực cho môi giới tại Việt Nam</Title>
                            <div className="py-4 mx-auto content" style={{ maxWidth: 500 }}>
                                ORIGO cung cấp nguồn bất động sản phong phú và website riêng miễn phí cùng hệ thống quản lý công việc thông minh,
                                giúp cho công việc môi giới đơn giản và nhanh chóng.
                            </div>
                            <div>
                                <button type="button" className="btn btn-info btn-bolder">Đăng Ký Môi Giới</button>
                            </div>
                            <div>
                                <Image publicId="home1" className="img-fluid" />
                            </div>
                        </div>
                    </div>

                    <div className={`row py-5 align-items-center justify-content-center ${style.model}`}>
                        <div className="col-12 text-center py-5">
                            <Title>Mô hình hoạt động</Title>
                        </div>
                        <div className="col-xl-4 text-center">
                            <h4 className="sub-title">Kho hàng chung</h4>
                            <p className="content">
                                Chứa nguồn bất động sản phong phú được chia sẻ từ các đối tác môi giới của ORIGO
                            </p>
                        </div>
                        <div className="col-xl-8 text-center">
                            <Image publicId="building" className="p-2 p-lg-4">
                                <Transformation width="68" crop="scale" />
                            </Image>
                            <Image publicId="building" className="p-2 p-lg-4">
                                <Transformation width="68" crop="scale" />
                            </Image>
                            <Image publicId="building" className="p-2 p-lg-4">
                                <Transformation width="68" crop="scale" />
                            </Image>
                            <Image publicId="building" className="p-2 p-lg-4">
                                <Transformation width="68" crop="scale" />
                            </Image>
                            <div></div>
                            <Image publicId="building" className="p-2 p-lg-4">
                                <Transformation width="68" crop="scale" />
                            </Image>
                            <Image publicId="building-full" className="p-2 p-lg-4">
                                <Transformation width="80" crop="scale" />
                            </Image>
                            <Image publicId="building" className="p-2 p-lg-4">
                                <Transformation width="68" crop="scale" />
                            </Image>
                            <Image publicId="building" className="p-2 p-lg-4">
                                <Transformation width="68" crop="scale" />
                            </Image>
                            <div className="text-center">
                                <button className="btn btn-info btn-sm" type="button">
                                    Nhận bán
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className={`row align-items-center justify-content-center`}>
                        <div className="col-xl-8 ml-xl-auto text-center">
                            <div className={style.matchArrow}></div>
                        </div>
                    </div>

                    <div className={`row py-5 align-items-center justify-content-center ${style.myStore}`}>
                        <div className="col-xl-4 text-center">
                            <h4 className="sub-title">Cửa hàng của tôi</h4>
                            <p className="content">
                                Chứa nguồn bất động sản phong phú được chia sẻ từ các đối tác môi giới của ORIGO
                            </p>
                        </div>
                        <div className="col-xl-8 text-center">
                            <Image publicId="building" className="p-2 p-lg-4" />
                            <Image publicId="building-full" className="p-2 p-lg-4" />
                            <Image publicId="building" className="p-2 p-lg-4" />
                            <div className="text-center">

                                <h5 className="text-muted">www.<span className="font-weight-bold">tencuaban</span>.origo.vn</h5>
                            </div>
                        </div>
                    </div>

                    <div className={`row align-items-center justify-content-center`}>
                        <div className="col-xl-8 ml-xl-auto text-center">
                            <div className={style.matchArrow}></div>
                        </div>
                    </div>

                    <div className={`row py-5 align-items-center justify-content-center ${style.uiUserPage}`}>
                        <div className="col-xl-4 text-center">
                            <h4 className="sub-title">Giao diện người mua</h4>
                            <p className="content mx-auto" style={{ maxWidth: 500 }}>
                                Khi người mua truy cập website của bạn dưới dạng <span className="text-muted d-inline">www.tencuaban.origo.vn</span> sẽ được tiếp cận một giao diện chuyên nghiệp,
                                đáng tin cậy với thông tin rõ ràng, chi tiết, giúp cho công việc mua bán dễ dàng hơn.
                            </p>
                        </div>
                        <div className="col-xl-8 text-center">
                            <Image publicId="department" className="p-2 p-lg-4" />
                        </div>
                        <div className="py-5">
                            <button className="btn btn-outline-info btn-bolder">
                                XEM CHI TIẾT CÁCH SỬ DỤNG
                            </button>
                        </div>
                    </div>

                    <div className="row py-5">
                        <div className="col-lg-6 text-center bg-light">
                            <Title>Quy trình hợp tác</Title>
                            <h4>Đăng ký tài khoản & nhận link website riêng miễn phí với các tiện ích:</h4>
                            <div className="row p-5">

                                <div class="col col-lg-6">
                                    <div class="card-body">
                                        This is some text within a card body.
                                    </div>
                                </div>
                                <div class="col col-lg-6 border-left">
                                    <div class="card-body">
                                        This is some text within a card body.
                                    </div>
                                </div>
                                <div class="col col-lg-6 border-top">
                                    <div class="card-body">
                                        This is some text within a card body.
                                    </div>
                                </div>
                                <div class="col col-lg-6 border-top border-left">
                                    <div class="card-body">
                                        This is some text within a card body.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 text-center bg-info">
                            <Title className="text-white">Cách thức giao dịch</Title>
                        </div>
                    </div>

                </div>
            </Fragment>
        );
    }
}

export default Home;