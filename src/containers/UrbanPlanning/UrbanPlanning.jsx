import React, { Component, Fragment } from 'react';
import { Title } from '../../components/Title';
import { TextField, SelectField } from '../../components/MdField'

import style from './UrbanPlanning.scss';

const LOCATIONS = [
    'Phường 1', 'Phường 2', 'Phường 3',
];

class UrbanPlanning extends Component {
    render() {
        return (
            <Fragment>
                <div className={`${style.planning} container mx-auto text-center p-5`}>
                    <Title>Kiểm tra quy hoạch</Title>
                    <div className="description p-4">
                        Công cụ này giúp bạn kiểm tra thông tin của quy hoạch BĐS mà bạn quan tâm
                    </div>

                    <form className={`row`} onSubmit={e => e.preventDefault()}>
                        <div className="col row align-items-center upload-wrap">
                            <div className="col">
                                <label className="upload" htmlFor="image">
                                    <i className="fal fa-camera fa-5x pt-4"></i>
                                    <div className="p-4 font-weight-bold">Tải ảnh chụp sổ hồng</div>
                                    <input type="file" name="image" id="image" className="d-none"/>
                                </label>

                            </div>
                        </div>
                        <div className="col-md-4 w-25 py-4 bg-light">
                            <h4 className="p-4">Tra cứu thông tin</h4>
                            <SelectField label="Phường/Xã">
                                <option value="">Chưa chọn</option>
                                {LOCATIONS.map((item, index) => (<option key={index} value={item}>{item}</option>))}
                            </SelectField>
                            <TextField label="Số tờ" placeholder="Tìm theo số hiệu tờ bản đồ"/>
                            <TextField label="Số thửa" placeholder="Tìm theo số thứ tự thửa đất"/>
                            <TextField label="Địa chỉ" placeholder="Tìm theo địa chỉ thửa đất"/>

                            <button className="btn btn-info btn-bolder btn-block btn-sm" type="submit">Kết Quả</button>
                            
                        </div>
                    </form>
                </div>

            </Fragment>
        )
    }
}

export default UrbanPlanning;