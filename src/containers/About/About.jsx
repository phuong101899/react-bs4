import React, { Component, Fragment } from 'react';
import { Title } from '../../components/Title';
import { Image } from '../../components/Image';

import style from './About.scss';

class About extends Component {

    render() {
        return (
            <Fragment>
                <div className={style.about}>

                    <div>
                        <Image publicId="about-banner1.png" className="img-fluid"></Image>
                    </div>
                    <div className="p-md-5">

                        <div className="row mx-0 p-2 p-md-5">
                            <div className="col-lg-8 mx-lg-auto content text-center">
                                <Title>Về ORIGO</Title>
                                <p className="text-center">
                                    ORIGO là giải pháp công nghệ bất động sản, định hình thị trường bằng cách kết nối tài nguyên và giá trị cho các bên tham gia.
                                    Chúng tôi thu thập, kiểm tra, thẩm định nhầm cung cấp, cố vấn giải pháp tìm kiếm, phân phối sản phẩm bất động sản.
                                    Giàu kinh nghiệm, giỏi chuyên môn, ORIGO thấu hiểu, tôn trọng, chia sẻ thông tin một cách xác thực và cởi mở.
                                </p>
                            </div>
                        </div>

                        <div className="row mx-0 p-2">
                            <div className="col-md-12 col-lg-5 ml-lg-auto">
                                <Image publicId="about-banner2.png" className="img-fluid"></Image>
                            </div>
                            <div className="col-md-12 col-lg-5 mr-lg-auto text-left">
                                <Title>Mục tiêu</Title>
                                <div className="content">
                                    <p>
                                        Tìm kiếm và giao dịch BĐS phiền phức, lo lắng, mệt mõi và mất thời gian? Hãy đơn giản điều đó.
                                    </p>
                                    <p>
                                        Giao nhà tận nơi, đó là mục tiêu làm cho ORIGO trở thành điểm kết nối đầu tư, nhân viên kinh doanh, môi giới bất động sản, người mua và bán nhà.
                                        Biến các giao dịch bất động sản trở nên dễ dàng và thú vị chỉ với các thao tác đơn giản trên nền tảng công nghệ thông minh.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row mx-0 p-2 p-md-5 text-center justify-content-center">
                            <div className="col-12">
                                <Title>Điều chúng tôi làm</Title>
                            </div>
                            <div className="card col-lg-5 col-xl-4 p-2 p-lg-4 m-2 m-xl-4 rounded shadow-sm border-0">
                                <div className="row no-gutters align-items-center">
                                    <div className="col-auto">
                                        <Image publicId="building-person.png" className="img-fluid"></Image>
                                    </div>
                                    <div className="col">
                                        <div className="card-body">
                                            <p className="card-text content">Chúng tôi trao lại cho bạn quyền lợi đã bị mất bằng cách loại bỏ thông tin giả, lược bớt quy trình để trả bất động sản về giá trị thực.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card col-lg-5 col-xl-4 p-2 p-lg-4 m-2 m-xl-4 rounded shadow-sm border-0">
                                <div className="row no-gutters align-items-center">
                                    <div className="col-auto">
                                        <Image publicId="building-person.png" className="img-fluid"></Image>
                                    </div>
                                    <div className="col">
                                        <div className="card-body">
                                            <p className="card-text content">Chúng tôi áp dụng công nghệ thông minh để tạo ra trải nghiệm chân thực, giúp việc tìm kiếm, giao dịch bất động sản trở nên thú vị. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12"></div>
                            <div className="card col-lg-5 col-xl-4 p-2 p-lg-4 m-2 m-xl-4 rounded shadow-sm border-0">
                                <div className="row no-gutters align-items-center">
                                    <div className="col-auto">
                                        <Image publicId="building-person.png" className="img-fluid"></Image>
                                    </div>
                                    <div className="col">
                                        <div className="card-body">
                                            <p className="card-text content">Chúng tối là điểm đến, là nơi kết nối chủ đầu tư, nhân viên kinh doanh, môi giới bất động sản, người mua và bán nhà.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card col-lg-5 col-xl-4 p-2 p-lg-4 m-2 m-xl-4 rounded shadow-sm border-0">
                                <div className="row no-gutters align-items-center">
                                    <div className="col-auto">
                                        <Image publicId="building-person.png" className="img-fluid"></Image>
                                    </div>
                                    <div className="col">
                                        <div className="card-body">
                                            <p className="card-text content">Chúng tôi mang các giải pháp phù hợp nhằm giúp giao dịch bất động sản đạt hiệu quả tối đa, rủi ro tối thiểu.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mx-0 p-2 p-md-5 text-center justify-content-center">
                            <div className="col-12">
                                <Title>Giá trị chúng tôi xây dựng</Title>
                            </div>
                            <div className="card col-lg-4 col-xl-3 p-2 mt-4 border-0">
                                <div>
                                    <Image publicId="building-person.png" className="img-fluid"></Image>
                                </div>
                                <div className="card-body shadow-sm rounded text-lg-left">
                                    <h3 className="font-weight-bold">Chúng tôi tôn trọng</h3>
                                    <p className="card-text content">
                                        ORIGO xây dựng và phát triển dựa vào sự cam kết, thấu hiểu và tôn trọng.
                                        Chúng tôi chia sẽ thông tin một cách nhất quán, minh bạch và xác thực.
                                    </p>
                                </div>
                            </div>
                            <div className="card col-lg-4 col-xl-3 p-2 mt-4 border-0">
                                <div>
                                    <Image publicId="building-person.png" className="img-fluid"></Image>
                                </div>
                                <div className="card-body shadow-sm rounded text-lg-left">
                                    <h3 className="font-weight-bold">Chúng tôi kiến tạo chuẩn mực</h3>
                                    <p className="card-text content">
                                        ORIGO xây dựng chuẩn mực và chuẩn mực trong mọi quy trình làm việc nhằm đem lại giá trị thực cho sản phẩm.
                                    </p>
                                </div>
                            </div>
                            <div className="card col-lg-4 col-xl-3 p-2 mt-4 border-0">
                                <div>
                                    <Image publicId="building-person.png" className="img-fluid"></Image>
                                </div>
                                <div className="card-body shadow-sm rounded text-lg-left">
                                    <h3 className="font-weight-bold">Chúng tôi đột phá</h3>
                                    <p className="card-text content">
                                        ORIGO nghiên cứu, khai thác tối đa công nghệ để mang đến các phương thức giao dịch bất động sản đột phá, đảm bảo tính an toàn, nhanh chóng, hiệu quả, tiết kiệm.
                                    </p>
                                </div>
                            </div>
                            <div className="col-12"></div>
                            <div className="card col-lg-4 col-xl-3 p-2 mt-4 border-0">
                                <div>
                                    <Image publicId="building-person.png" className="img-fluid"></Image>
                                </div>
                                <div className="card-body shadow-sm rounded text-lg-left">
                                    <h3 className="font-weight-bold">Chúng tôi lý trí</h3>
                                    <p className="card-text content">
                                        ORIGO nhìn nhận sự việc dựa trên cơ sở khoa học, sự logic và hợp lý.
                                        Chúng tôi tìm hiểu, phân tích các vấn đề và đưa ra giải pháp xử lý hiệu quả.
                                    </p>
                                </div>
                            </div>
                            <div className="card col-lg-4 col-xl-3 p-2 mt-4 border-0">
                                <div>
                                    <Image publicId="building-person.png" className="img-fluid"></Image>
                                </div>
                                <div className="card-body shadow-sm rounded text-lg-left">
                                    <h3 className="font-weight-bold">Chúng tôi độc đáo</h3>
                                    <p className="card-text content">
                                        ORIGO xây dựng các hệ giá trị khác biệt, tao ra sự độc đáo bằng cách kết nối khả năng sáng tạo và sức mạnh tinh thần của các cá thể.
                                    </p>
                                </div>
                            </div>
                            <div className="card col-lg-4 col-xl-3 p-2 mt-4 border-0">
                                <div>
                                    <Image publicId="building-person.png" className="img-fluid"></Image>
                                </div>
                                <div className="card-body shadow-sm rounded text-lg-left">
                                    <h3 className="font-weight-bold">Chúng tôi dẫn đầu</h3>
                                    <p className="card-text content">
                                        ORIGO đam mê, công hiến, chứng minh mình dẫn đầu bằng hành động và kết quả.
                                        Chúng tôi nói điều chúng tôi làm và làm điều chúng tôi nói.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default About;