import React, { Component, Fragment } from 'react';
import { Header } from './components/Header';
import { Footer } from './components/Footer';
import { Home } from './containers/Home';
import { UrbanPlanning } from './containers/UrbanPlanning';
import { About } from './containers/About';
import { BrowserRouter as Router, Route } from "react-router-dom";


import './App.scss';


class App extends Component {
    render() {
        return (
            <Router>
                
                <div className="container-fluid px-0">
                    <Header />
                    <Route path="/" exact component={Home} />
                    <Route path="/quy-hoach/" component={UrbanPlanning} />
                    <Route path="/gioi-thieu/" component={About} />
                    <Footer />

                </div>

            </Router>
        );
    }
}

export default App;
