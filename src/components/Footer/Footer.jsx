import React, { Component, Fragment } from 'react';
import { Link } from "react-router-dom";

import style from './Footer.scss';

class Footer extends Component {
    render() {
        return (
            <Fragment>
                <footer className={`${style.footer} mt-5`}>
                    <div className="row justify-content-center m-0">
                        <div className="col-sm-12 p-5 col-lg-auto text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth={2} strokeLinecap="round" strokeLinejoin="round"><circle cx={12} cy={12} r={10} /><line x1="14.31" y1={8} x2="20.05" y2="17.94" /><line x1="9.69" y1={8} x2="21.17" y2={8} /><line x1="7.38" y1={12} x2="13.12" y2="2.06" /><line x1="9.69" y1={16} x2="3.95" y2="6.06" /><line x1="14.31" y1={16} x2="2.83" y2={16} /><line x1="16.62" y1={12} x2="10.88" y2="21.94" /></svg>
                            <h5>ORIGO</h5>
                        </div>
                        <div className="col-sm-12 p-5 col-lg-auto text-center text-lg-left">
                            <h5 className="title">LIÊN HỆ</h5>
                            <ul className="list-unstyled text-small">
                                <li className="py-1"><a className="text-primary-ecoe font-weight-bold" href="#"><i className="fas fa-phone fa-rotate-90"></i><span className="mx-2">0123 456 789</span></a></li>
                                <li className="py-1"><a className="text-muted" href="#"><i className="far fa-envelope text-primary-ecoe"></i><span className="mx-2">orgio@gmail.com</span></a></li>
                                <li className="py-1"><a className="text-muted" href="#"><i className="fal fa-map-marker-alt text-primary-ecoe"></i><span className="mx-2">123 Hàn Thuyên, Phường 6, Quận 3, TPHCM</span></a></li>

                            </ul>
                        </div>
                        <div className="col-sm-12 p-5 col-lg-auto text-center text-lg-left">
                            <h5 className="title">THÔNG TIN</h5>
                            <ul className="list-unstyled text-small">
                                <li className="py-1"><a className="text-muted" href="#">Trang chủ</a></li>
                                <li className="py-1"><a className="text-muted" href="#">Thông tin hỗ trợ</a></li>
                                <li className="py-1"><a className="text-muted" href="#">Kiểm tra quy hoạch</a></li>
                                <li className="py-1"><Link className="text-muted" to="/gioi-thieu">Về ORIGO</Link></li>
                                <li className="py-1"><a className="text-muted" href="#">Blog tin tức</a></li>
                            </ul>
                        </div>
                        <div className="col-sm-12 p-5 col-lg-auto text-center text-lg-left">
                            <h5 className="title">ỨNG DỤNG</h5>
                            <ul className="list-unstyled text-small">
                                <li className="py-1"><a className="text-muted" href="#">ORIGO trên IOS</a></li>
                                <li className="py-1"><a className="text-muted" href="#">ORIGO trên Android</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>

            </Fragment>
        );
    }
}

export default Footer;