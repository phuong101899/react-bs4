import React, { Component, Fragment } from 'react';
import { NavLink as Link, BrowserRouter as Router } from "react-router-dom";
import style from './Header.scss';

const MENU_LIST = [
    {
        name: 'TRANG CHỦ',
        route: '/',
        active: true
    },
    {
        name: 'THÔNG TIN HỖ TRỢ',
        route: '/thong-tin-ho-tro'
    },
    {
        name: 'KIỂM TRA QUY HOẠCH',
        route: '/quy-hoach'
    }
];

class Header extends Component {
    render() {
        return (
            <Fragment>
                <nav className={`navbar navbar-expand-lg navbar-light bg-white sticky-top ${style.header}`}>
                    <Link className="navbar-brand logo-text" to="/">ORIGO</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ml-auto">
                           
                            {MENU_LIST.map((item, index) => (
                                <li className="nav-item mr-xl-4" key={index}>
                                    <Link className={`nav-link`} activeClassName="active" exact to={item.route}>{item.name}</Link>
                                </li>
                            ))}
                            
                            <li className="nav-item mr-xl-4">
                                <a href="tel:0123456789" className={`nav-link phone`}><i className="fas fa-phone fa-rotate-90"></i> <span>0123 456 789</span></a>
                            </li>
                        </ul>
                        <form className="form-inline col-md-auto col-sm-12 px-0">
                            <div className="form-group mr-2 mx-xl-4 mx-lg-2">
                                <button type="button" className="btn btn-info btn-bolder">Đăng Ký Môi Giới</button>
                            </div>
                            <div className="form-group">
                                <button type="button" className="btn btn-outline-info btn-bolder">MUA BẤT ĐỘNG SẢN</button>
                            </div>
                            
                        </form>
                    </div>
                </nav>

            </Fragment>
        );
    }
}

export default Header;