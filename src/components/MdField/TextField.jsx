import React, { Component, Fragment } from 'react';
import style from './MdField.scss';

class TextField extends Component {

    render () {
        return (
            <Fragment>
                <div className={`form-group text-left ${style['text-field']}`}>
                    <label className="font-weight-bold">{this.props.label}</label>
                    <input type="text" className="form-control" placeholder={this.props.placeholder}/>
                </div>
            </Fragment>
        )
    }
}

export default TextField;