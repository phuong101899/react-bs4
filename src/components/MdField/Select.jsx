import React, { Component, Fragment } from 'react';
import style from './MdField.scss';


class SelectField extends Component {

    componentDidMount(){
        this.id = (new Date()).getMilliseconds();
    }

    render() {
        return (
            <Fragment>
                <div className={`form-group text-left ${style.select}`}>
                    <label className="font-weight-bold" htmlFor={this.id}>{this.props.label}</label>
                    <select name={this.props.name} id={this.id} className="form-control">
                        {this.props.children}
                    </select>
                </div>
            </Fragment>
        );
    }
}

export default SelectField;