import React, { Component, Fragment } from 'react';
import { Image } from 'cloudinary-react';
import config from '../../config.json';


class ImageComponent extends Component {
    render(){
        
        return (
            <Fragment>
                <Image {...this.props} cloudName={config.cloudName}>
                    {this.props.children}
                </Image>
            </Fragment>
        );
    }
}

export default ImageComponent;