import React, { Component, Fragment } from 'react';
import style from './Title.scss';
class Title extends Component {
    render() {
        return (
            <Fragment>
                <span className={style.title}>{this.props.children}</span>
            </Fragment>
        );
    }
}

export default Title;